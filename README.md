# Cookiecutter PyPackage

Cookiecutter template for Daiquiri local implmentation.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

```pip install -U cookiecutter```

Generate a Python package project::

```cookiecutter https://gitlab.esrf.fr/ui/daiquiri-local.git```

Then:

* Create a git repository: `git init .`
* Install the package into the local conda environment `pip install -e .`
* This package can then be used to launch daiquiri with the correct resources and implementors `daiquiri-server-idxx`
