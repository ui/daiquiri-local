# !/usr/bin/env python
import sys
from setuptools import setup, find_packages

TESTING = any(x in sys.argv for x in ["test", "pytest"])

with open("README.md") as readme_file:
    readme = readme_file.read()

requirements = ["daiquiri"]

setup_requirements = ["pytest-runner", "pytest"] if TESTING else []

test_requirements = ["pytest-cov", "mock"]


from distutils.core import setup

setup(
    name="cookiecutter-daiquiri-pypackage",
    packages=[],
    version="0.1.0",
    description="Cookiecutter template for ESRF daiquiri acqusition framework",
    long_description=readme,
    long_description_content_type="text/markdown",
    author="BCU Team",
    license="GNU Lesser General Public License v3",
    url="http://gitlab.esrf.fr/ui/daiquiri-local",
    keywords=["cookiecutter", "template", "package", "beamline", "ESRF"],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Topic :: Software Development",
    ],
)
