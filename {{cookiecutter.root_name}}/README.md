# Development

## Install

Editable install (needed to load actor implementors, see `daiquiri_{{cookiecutter.project_slug}}/resources/config/config.yml#implementors`)

```
python -m pip install --no-deps -e .
```

## Run REST server

Start server with console script

```
daiquiri-server-{{cookiecutter.project_slug}} --static-folder=... --hardware-folder=...
```

or execute the python module

```
python -m daiquiri_{{cookiecutter.project_slug}} --static-folder=... --hardware-folder=...
```

The server configuration can be found in `daiquiri_{{cookiecutter.project_slug}}/resources/config`.
