# Daiquiri Documentation

## Start REST server

```
daiquiri-server-{{cookiecutter.project_slug}}
```

 * **static-folder**: web client static pages
 * **hardware-folder**: hardware repository

Server resources will be searched for in `daiquiri_{{cookiecutter.project_slug}}.resources` followed by `daiquiri.resources`.
