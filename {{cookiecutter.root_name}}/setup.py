#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
import re
from collections import OrderedDict

from setuptools import setup, find_packages

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

with io.open("{{cookiecutter.project_slug}}/__init__.py", "rt", encoding="utf8") as f:
    version = re.search(r"__version__ = \"(.*?)\"", f.read()).group(1)

setup(
    name="{{cookiecutter.project_slug}}",
    version=version,
    license="GPLv3",
    author="",
    author_email="",
    maintainer="",
    maintainer_email="",
    description="",
    long_description=readme,
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    zip_safe=False,
    platforms="Linux",
    python_requires=">= 3.6",
    install_requires=[
        "gevent",
        "flask",
        "python-socketio",
        "python-engineio",
        "flask-socketio",
        "gevent-websocket",
    ],
    extras_require={
        "dev": ["pytest>=3", "pytest-cov", "coverage", "sphinx"],
        "docs": ["sphinx"],
    },
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points={
        "console_scripts": [
            "daiquiri-server-{{cookiecutter.beamline.lower()}} = {{cookiecutter.project_slug}}.app:main"
        ]
    },
)
