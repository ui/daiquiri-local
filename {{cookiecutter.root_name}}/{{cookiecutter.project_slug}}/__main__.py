# -*- coding: utf-8 -*-

# Allows running the REST server with "python -m daiquiri_{{cookiecutter.project_slug}}"
from daiquiri_{{cookiecutter.project_slug}}.app import main

main()
