#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
from daiquiri.app import run_server


def main():
    """Runs REST server with CLI configuration
    """
    root = os.path.dirname(__file__)
    # Currently not accepting additional resource folders from CLI
    resource_folder = os.path.join(root, "resources")
    # Default hardware folder (TODO: what is this for?)
    hardware_folder = os.path.join(root, "resources", "external")
    if not os.path.isdir(hardware_folder):
        hardware_folder = ""

    parser = argparse.ArgumentParser(description="REST server for the ID21 BLISS GUI")
    parser.add_argument(
        "--static-folder",
        dest="static_folder",
        default="static.default",
        help="Web server static folder (static.* refers to server resource)",
    )
    parser.add_argument(
        "--hardware-folder",
        dest="hardware_folder",
        default=hardware_folder,
        help="Hardware folder",
    )
    parser.add_argument(
        "-p", "--port", dest="port", default=8080, help="Web server port", type=int
    )
    args = parser.parse_args()
    run_server(
        resource_folders=[resource_folder],
        implementors="{{cookiecutter.project_slug}}.implementors",
        static_folder=args.static_folder,
        hardware_folder=args.hardware_folder,
        port=args.port,
        static_url_path="/",
    )


if __name__ == "__main__":
    main()
