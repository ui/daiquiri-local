"""Component actor implementations. Actors are implemented
in a submodule with the component's name.
"""
